<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
?>
<article class="<?php print $classes; ?> clearfix node-<?php print $node->nid; ?>" <?php print $attributes; ?>>
	<?php if ($title_prefix || $title_suffix || $display_submitted || $unpublished || $preview || !$page && $title): ?>
	<header>
		<?php print render($title_prefix); ?>
		<?php if (!$page && $title): ?>
		<h2<?php print $title_attributes; ?>>
			<a href="<?php print $node_url; ?>">
				<?php print $title; ?>
			</a>
			</h2>
			<?php endif; ?>
			<?php print render($title_suffix); ?>
			<?php if ($display_submitted): ?>
			<p class="submitted">
				<?php print $user_picture; ?>
				<?php print $submitted; ?>
			</p>
			<?php endif; ?>
			<?php if ($unpublished): ?>
			<mark class="watermark">
				<?php print t('Unpublished'); ?>
			</mark>
			<?php elseif ($preview): ?>
			<mark class="watermark">
				<?php print t('Preview'); ?>
			</mark>
			<?php endif; ?>
	</header>
	<?php endif; ?>
	<?php if(render($content['field_facebook'])&&render($content['field_twitter'])):?>
	<?php endif; ?>
	<!-- <div class="section social-feeds">
			<div class="container">
				<div class="section social-content">
				<div class="med-title">Social Media Feeds</div>
				<div class="social-content-wrap">
				 <ul style="text-align: center;">
					 <li style="display: inline-block;padding: 20px">
						 <div class="social-content social-content-fb">
								<div class="social-feed social-content-facebook">
									<div class="fb-page" data-href="https://www.facebook.com/FDCP.ph/" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/FDCP.ph/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/FDCP.ph/">Film Development Council of the Philippines</a></blockquote></div>
									<div id="fb-root"></div>
								</div>
							</div>
					 </li>
					 <li style="display: inline-block;padding: 20px;">
						 <div class="social-content social-content-tweeter"><a class="twitter-timeline" data-height="500" data-width="500" href="https://twitter.com/FDCPH">Tweets by FDCPH</a>
								<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
							</div>
					 </li>
				 </ul>
					 

				
				</div>
			</div>
			</div>
		</div> -->
	<?php if (render($content['field_block_id'])): ?>
	<div class="block-wrapper">
		<?php 
			$block_id = $content['field_block_id']['#items'][0]['value'];
			$my_block = module_invoke('block', 'block_view', $block_id);
			print render($my_block['content']);
		?>
	</div>
	<?php endif; ?>
	<?php if (render($content['field_description_accordion'])): ?>	
	<div class="section full-description" style="background: #<?php if(render($content['field_description_bacground_hex'])): ?><?php print render($content['field_description_bacground_hex'][0]['#markup']); ?><?php else: ?>eee<?php endif; ?>; color: #<?php if(render($content['field_description_text_color_hex'])): ?><?php print render($content['field_description_text_color_hex'][0]['#markup']); ?><?php else: ?>333<?php endif; ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-6 fright">
					<div class="content-right">
						<div class="img-wrap">
							<?php print render($content['field_image_right']); ?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="content-left">
						<div class="med-title"><?php echo t('Description');?></div>
						<div class="small-desc">
							<?php print render($content['field_description_accordion']); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<?php if (render($content['body'])): ?>
		<div class="section section-body">
			<div class="container">
				<?php print render($content['body']); ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if($field_contact_number=render($content['field_contact_number']) && $field_facebook=render($content['field_facebook']) && $field_email=render($content['field_email']) && $field_twitter=render($content['field_twitter'])): ?>
		<div class="section section-contact">
			<div class="container">
				<div class="description">
					<ul>
						<?php if($field_contact_number): ?>
						<li class="render-contact"><i class="fa fa-phone"></i>
							<?php print render($content['field_contact_number']); ?>
						</li>
						<?php endif; ?>
						<?php if($field_facebook):?>
						<li class="render-fb"><i class="fa fa-facebook"></i>
							<?php print render($content['field_facebook']); ?>
						</li>
						<?php endif; ?>
						<?php if($field_email):?>
						<li class="render-fb"><i class="fa fa-envelope"></i>
							<?php print render($content['field_email']); ?>
						</li>
						<?php endif; ?>
						<?php if($field_twitter):?>
						<li class="render-fb"><i class="fa fa-twitter"></i>
							<?php print render($content['field_twitter']); ?>
						</li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php
		// We hide the comments and links now so that we can render them later.
		hide($content['comments']);
		hide($content['links']);
		hide($content['field_image_right']);
		hide($content['body']);
		hide($content['field_block_id']);
		hide($content['field_description_text_color_hex']);
		hide($content['field_description_bacground_hex']);
		hide($content['field_description_accordion']);
		hide($content['field_program_location']);
		hide($content['field_program_numbers']);
		print render($content);
	?>
	<?php print render($content['links']); ?>
	<?php print render($content['comments']); ?>

	<?php if(render($content['field_program_location'])||render($content['field_program_numbers'])):?>
	<div class="section full-description centered-text">
		<div class="container">
			<?php if(render($content['field_program_location'])):?>
				<p><?php print render($content['field_program_location']); ?></p>
			<?php endif; ?>
			<?php if(render($content['field_program_numbers'])):?>
				<p><?php print render($content['field_program_numbers']); ?></p>
			<?php endif; ?>
		</div>
	</div>
	<?php endif; ?>
</article>