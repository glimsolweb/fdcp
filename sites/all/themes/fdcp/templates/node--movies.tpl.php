<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
?>
	<article class="container <?php print $classes; ?> clearfix node-<?php print $node->nid; ?>" <?php print $attributes; ?>>
		<?php if ($title_prefix || $title_suffix || $display_submitted || $unpublished || $preview || !$page && $title): ?>
		<header>
			<?php print render($title_prefix); ?>
			<?php if (!$page && $title): ?>
			<h2<?php print $title_attributes; ?>>
				<a href="<?php print $node_url; ?>">
					<?php print $title; ?>
				</a>
				</h2>
				<?php endif; ?>
				<?php print render($title_suffix); ?>
				<?php if ($display_submitted): ?>
				<p class="submitted">
					<?php print $user_picture; ?>
					<?php print $submitted; ?>
				</p>
				<?php endif; ?>
				<?php if ($unpublished): ?>
				<mark class="watermark">
					<?php print t('Unpublished'); ?>
				</mark>
				<?php elseif ($preview): ?>
				<mark class="watermark">
					<?php print t('Preview'); ?>
				</mark>
				<?php endif; ?>
		</header>
		<?php endif; ?>
	 
	 <?php /* 
		<div class="main-movie">
				<div class="section1">
					<div class="row">
						<div class="col-md-4">
							<div class="content-left">
								<div class="img-wrap">
									<div class="img-mask-light"></div>
									<?php if(render($content['field_movie_image'])): ?>
													<?php print render($content['field_movie_image']); ?>
												<?php endif; ?></div>
							</div>
						</div>
						<div class="col-md-8">
							<div class="content-right">
								<div class="img-wrap">
										<?php if($field_video_embed_field_image_preview_alt = render($content['field_video_embed_field_image_preview_alt'])): ?>
											<?php print $field_video_embed_field_image_preview_alt; ?>
										<?php endif; ?>
								</div>
									<a href="[field_video_embed_field_video_clip_video_url]" class="video-popup-link"><img src="/fdcp-revamp/sites/default/files/playicon-big.png"></a>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>
	
		*/?>
		<?php //echo '<pre>' . print_r($content['field_date_released']['#items'][0],1) . '<pre>'; ?>
		<?php /*
			$year = date('Y', strtotime($content['field_date_released']['#items'][0]['value']));
			
		?>
		<div class="movie-title">
			<div class="med-title"><h2><?php print $title; ?> <?php if (render($content['field_date_released'])): ?>(<?php echo $year; ?>)<?php endif; ?></h2></div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="content-left">
					<div class="img-wrap">
						<div class="img-mask"></div>[field_movie_image]</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="content-right">
					<div class="img-wrap">
						[field_video_embed_field_image_preview_alt]
						<a href="[field_video_embed_field_video_clip_video_url]" class="video-popup-link"><i class="fa fa-play-circle"></i></a>
					</div>
				</div>
			</div>
		</div>
		*/ ?>

		<?php
		// We hide the comments and links now so that we can render them later.
		hide($content['comments']);
		hide($content['links']);
		hide($content['title']);
		hide($content['field_movie_image']);
		hide($content['field_specials']);
		hide($content['field_genra']);
		hide($content['field_movie_rating']);
		hide($content['field_duration']);
		hide($content['field_movie_type']);
		hide($content['field_director']);
		hide($content['field_writers']);
		hide($content['field_date_released']);
		hide($content['field_trailer_iframe']);
		hide($content['field_video_clips']);
		hide($content['field_rating']);
		hide($content['field_stars']);
		hide($content['field_date']);
		hide($content['body']);
		hide($content['path']);
		hide($content['field_photos']);
		hide($content['field_film_location']);  
		hide($content['field_cinematheque_branch']);  
		hide($content['field_video_embed']);  
		print render($content);
	?>
			<?php print render($content['links']); ?>
			<?php print render($content['comments']); ?>
	</article>