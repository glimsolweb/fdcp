<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
?>
<article class="<?php print $classes; ?> clearfix node-<?php print $node->nid; ?>"<?php print $attributes; ?>>

	<?php if ($title_prefix || $title_suffix || $display_submitted || $unpublished || $preview || !$page && $title): ?>
		<header>
			<?php /*print render($title_prefix); ?>
			<?php if (!$page && $title): ?>
				<h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
			<?php endif; ?>
			<?php print render($title_suffix); ?>

			<?php if ($display_submitted): ?>
				<p class="submitted">
					<?php print $user_picture; ?>
					<?php print $submitted; ?>
				</p>
			<?php endif; ?>

			<?php if ($unpublished): ?>
				<mark class="watermark"><?php print t('Unpublished'); ?></mark>
			<?php elseif ($preview): ?>
				<mark class="watermark"><?php print t('Preview'); ?></mark>
			<?php endif;*/ ?>
		</header>
	<?php endif; ?>


	<div class="container">
		<div id="media-wrapper">

			<?php if(render($content['field_image_preview'])): ?>
				<div class="img-wrap"><?php print render($content['field_image_preview']); ?></div>
			<?php endif; ?>

			<div class="reg-title">
				<?php echo $title; ?>
				<?php if(render($content['field_date'])): ?>
					<div class="tiny-text"><?php print render($content['field_date'][0]['#markup'],1); ?></div>
				<?php endif; ?>
			</div>

			<div class="body"><?php print render($content['body'][0]['#markup'],1); ?></div>

			<?php if(render($content['field_other_images'])): ?>
				<div class="other-img-wrap"><?php print render($content['field_other_images']); ?></div>
			<?php endif; ?>

			<div class="sharethis-title">Share</div>
			<?php
				$block = module_invoke('sharethis', 'block_view', 'sharethis_block');
				print render($block['content']);
			?>

			<?php 
				$my_block = module_invoke('views', 'block_view', 'media-block_15');
				print render($my_block['content']);
			?>
		</div>
	</div>

	<?php
		// We hide the comments and links now so that we can render them later.
		hide($content['comments']);
		hide($content['links']);
		hide($content['field_image_preview']);
		hide($content['field_date']);
		hide($content['field_other_images']);
		hide($content['field_media_type']);
		hide($content['field_related_to_']);
		hide($content['body']);
		print render($content);
	?>

	<?php print render($content['links']); ?>

	<?php print render($content['comments']); ?>

</article>
