<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>

<div class="sb-site sb-slide">
<div class="primary-head">
  

<div class="top-head" style="width: 100%;z-index: 21">
    <div class="container">
      <?php print render($page['header']); ?> </div>
  </div>
 <header class="header " role="banner">
    <div class="container">
      <div class="head-wrapper">
        <div class="top-social"> </div>
        <div class="top-navigation">
          <div class="nav-left" style="float">
            <?php if ($logo): ?>
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__logo">
                <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="header__logo-image" />
                <?php print render($page['site_name']); ?>
              </a>
            <?php endif; ?>
          </div>
          <div class="nav-middle" style="text-align: right; ">
            <?php print render($page['navigation']); ?>
            <div class="primary-navigation">
                <?php $primary_navigation = render($page['primary_navigation']); ?>
                <?php if($primary_navigation): ?>
                <?php print $primary_navigation; ?>
                <?php endif; ?>
            </div>
          </div>
          <div class="nav-right">
            <?php $secondary_navigation = render($page['secondary_navigation']); ?>
            <?php if($secondary_navigation): ?>
            <?php print $secondary_navigation; ?>
            <?php endif; ?> </div>
        </div>
      </div>
      <div class="sb-toggle-right navbar-lefts"><i class="fa fa-bars">&nbsp;</i><i class="fa fa-times">&nbsp;</i></div>
    </div>
  </header>
  </div>
  


  <!-- Render Page Banner -->
    <div class="page-banner">
      <div class="container">
        <?php $page_banner = render($page['page_banner']); ?>
        <?php if($page_banner): ?>
        <?php print $page_banner; ?>
        <?php endif; ?> </div>
    </div>
    <div class="page-banner-mobile">
      <?php $page_banner = render($page['page_banner']); ?>
      <?php if($page_banner): ?>
      <?php print $page_banner; ?>
      <?php endif; ?> 
    </div>
<div class="container  main-content-wrapper">
  

  <div class="row">

    <?php
      // Render the sidebars to see if there's anything in them.
      $sidebar_first  = render($page['sidebar_first']);
      $sidebar_second = render($page['sidebar_second']);
      // Decide on layout classes by checking if sidebars have content.
      $content_class = 'layout-3col__full';
      $sidebar_first_class = $sidebar_second_class = '';
      if ($sidebar_first && $sidebar_second):
        $content_class = 'col-md-4';
        $sidebar_first_class = 'col-md-4';
        $sidebar_second_class = 'col-md-4';
      elseif ($sidebar_second):
        $content_class = 'col-md-8';
        $sidebar_second_class = 'col-md-4';
      elseif ($sidebar_first):
        $content_class = 'col-md-8';
        $sidebar_first_class = 'col-md-4';
      endif;
    ?>
      <div class="container">
        <?php print $breadcrumb; ?>
      </div>
      
      <?php if ($sidebar_first): ?>
      <aside class="<?php print $sidebar_first_class; ?>" role="complementary">
        <?php print $sidebar_first; ?>
      </aside>
    <?php endif; ?>

    <div class="container">
      <?php print render($page['top_content']); ?> 
    </div>
    <main class="<?php print $content_class; ?> main-container" role="main">
      
      <?php print render($page['highlighted']); ?>
      
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php print render($tabs); ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
    </main>

    <?php if ($sidebar_second): ?>
      <aside class="<?php print $sidebar_second_class; ?>" role="complementary">
        <?php print $sidebar_second; ?>
      </aside>
    <?php endif; ?>

  </div>
</div>

  <div class="full-width">
      <?php $main_content = render($page['main_content']); ?>
        <?php if($main_content): ?>
        <?php print $main_content; ?>
        <?php endif; ?>
  </div>
  <div class="container">
    <?php print render($page['bottom_content']); ?> 
  </div>



<?php print render($page['bottom']); ?>
  <div class="footer-wrapper">
    <div class="container">
      <?php print render($page['footer']); ?> </div>
  </div>

  <div class="footer-bottom"> 
      <?php print render($page['footer_bottom']); ?> 
  </div>

</div>

<div class="sb-slidebar sb-right sb-style-push sb-momentum-scrolling">
  <?php print render($page['side_bar_nav']); ?> 
</div>
<?php print render($page['modal']); ?>