<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
?>
<article class="<?php print $classes; ?> clearfix node-<?php print $node->nid; ?>"<?php print $attributes; ?>>

  <?php if ($title_prefix || $title_suffix || $display_submitted || $unpublished || $preview || !$page && $title): ?>
    <header>
      <?php print render($title_prefix); ?>
      <?php if (!$page && $title): ?>
        <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
      <?php endif; ?>
      <?php print render($title_suffix); ?>

      <?php if ($display_submitted): ?>
        <p class="submitted">
          <?php print $user_picture; ?>
          <?php print $submitted; ?>
        </p>
      <?php endif; ?>

      <?php if ($unpublished): ?>
        <mark class="watermark"><?php print t('Unpublished'); ?></mark>
      <?php elseif ($preview): ?>
        <mark class="watermark"><?php print t('Preview'); ?></mark>
      <?php endif; ?>
    </header>
  <?php endif; ?>

  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
  ?>

  <?php // print render($content['field_fdcp_event']) ?>
  <?php // echo '<pre>field_fdcp_event - '.print_r($content['field_fdcp_event']['#object']->field_fdcp_event['und']['0']['value'],1).'</pre>'; ?>
  <?php if(isset($content['field_fdcp_event']['#object']->field_fdcp_event['und']['0']['value']) && $content['field_fdcp_event']['#object']->field_fdcp_event['und']['0']['value']=='1'): ?>
    <div class="biff-page-content">
      <div class="container">
        <div class="biff-item">
          <?php if(render($content['field_date'])): ?>
            <div class="date-wrap"><?php print render($content['field_date']); ?></div>
          <?php endif; ?>
          <div class="title-wrap"><?php echo $title;?></div>
          <?php if(render($content['field_subtitle'])): ?>
            <div class="subtitle-wrap"><?php print render($content['field_subtitle']); ?></div>
          <?php endif; ?>
          <?php if(render($content['field_event_location'])): ?>
            <div class="location-wrap"><?php print render($content['field_event_location']); ?></div>
          <?php endif; ?>
          <?php if(render($content['field_event_time'])): ?>
            <div class="time-wrap"><?php print render($content['field_event_time']); ?></div>
          <?php endif; ?>
          <?php if(render($content['body'])): ?>
            <div class="desc-wrap"><?php print render($content['body']); ?></div>
          <?php endif; ?>
        </div>
        <div class="fdcp-event-form-wrapper">
          <?php  print render($content); ?>
        </div>
      </div>
    </div>
  <?php else: ?>
    <div class="container">  
      <?php  print render($content); ?>
    </div>
  <?php endif; ?>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>

</article>
