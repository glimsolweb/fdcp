// Get Instagram Feeds
// var userFeed = new Instafeed({
// 	get: 'user',
// 	userId: '3706973297',
// 	accessToken: '3706973297.1677ed0.b40fbff40ec54798a53e49eec49f3ee9',
// 	template: '<div class="insta-feed"><div class="insta-desc"> {{caption}} </div><a class="animation" href="{{link}}"><img src="{{image}}" /></a> <div class="insta-likes"> <span class="fa fa-heart"> {{likes}} </span> <span class="fa fa-comments"> {{comments}} </span> </div>  </div>'
// 	});
// userFeed.run();
	var getUrlParameter = function getUrlParameter(sParam) {
		var sPageURL = decodeURIComponent(window.location.search.substring(1)),
			sURLVariables = sPageURL.split('&'),
			sParameterName,
			i;

		for (i = 0; i < sURLVariables.length; i++) {
			sParameterName = sURLVariables[i].split('=');

			if (sParameterName[0] === sParam) {
				return sParameterName[1] === undefined ? true : sParameterName[1];
			}
		}
	}; 

function equalizeHeights(selector) {
	var heights = new Array();
	jQuery(selector).each(function() {
		jQuery(this).css('min-height', '0');
		jQuery(this).css('max-height', 'none');
		jQuery(this).css('height', 'auto');
		heights.push(jQuery(this).outerHeight());
	});

	var max = Math.max.apply(Math, heights);
	jQuery(selector).each(function() {
		jQuery(this).css('height', max + 'px');
	});
}


// Owl Carousel
jQuery(document).ready(function() {

	var eventTime = getUrlParameter('event_time_option');
	jQuery('#webform-client-form-302 #edit-submitted-event-time-option .form-type-checkbox').hide();
	if(eventTime && eventTime!=''){
		// console.log('eventTime'+eventTime);
		eventTime = eventTime.toString();
		var eventTimeSplit = eventTime.split('|');
console.log('eventTimeSplit.length'+eventTimeSplit.length);
		// console.log('eventTimeSplit '+eventTimeSplit[0]);
		// console.log('eventTimeSplit '+eventTimeSplit[1]);
		jQuery('#webform-client-form-302 #edit-submitted-event-time-option .form-type-checkbox').each(function(){
		var inputVal = jQuery(this).find('input').val();
for(eventX=0;eventX<eventTimeSplit.length;eventX++){
		if(eventTimeSplit[eventX]==inputVal){
			jQuery(this).show();
		}

	}
		console.log('inputVal '+inputVal);
		});
	}
	jQuery.slidebars();

	jQuery(window).load(function() {
		equalizeHeights(jQuery('.page-banner-mobile .banner-overlay'));
	});
	jQuery(window).resize(function() {
		setTimeout(function() {
			equalizeHeights(jQuery('.page-banner-mobile .banner-overlay'));
		}, 120);
	});


//Default Slider
jQuery(".spotlight-slider").owlCarousel({

	// Most important owl features
	items: 3,
	itemsCustom: false,
	itemsDesktop: [1199, 3],
	itemsDesktopSmall: [980, 3],
	itemsTablet: [768, 3],
	itemsTabletSmall: false,
	itemsMobile: [479, 3],
	singleItem: false,
	itemsScaleUp: false,

	//Basic Speeds
	slideSpeed: 1000,
	paginationSpeed: 1000,
	rewindSpeed: 1000,

	//Autoplay
	autoPlay: 10000,
	stopOnHover: false,

	// Navigation
	navigation: false,
	navigationText: ["<i class='fa fa-angle-left'>", "<i class='fa fa-angle-right'>"],
	rewindNav: true,
	scrollPerPage: false,

	//Pagination
	pagination: true,
	paginationNumbers: false,

	// Responsive 
	responsive: true,
	responsiveRefreshRate: 200,
	responsiveBaseWidth: window,

	// CSS Styles
	baseClass: "owl-carousel",
	theme: "owl-theme",

	//Lazy load
	lazyLoad: false,
	lazyFollow: true,
	lazyEffect: "fade",

	//Auto height
	autoHeight: false,

	//JSON 
	jsonPath: false,
	jsonSuccess: false,

	//Mouse Events
	dragBeforeAnimFinish: true,
	mouseDrag: true,
	touchDrag: true,

	//Transitions
	transitionStyle: false,

	// Other
	addClassActive: false,

	//Callbacks
	beforeUpdate: false,
	afterUpdate: false,
	beforeInit: false,
	afterInit: false,
	beforeMove: false,
	afterMove: false,
	afterAction: false,
	startDragging: false,
	afterLazyLoad: false
});

//Banner Slider
jQuery(".banner-slider").owlCarousel({

	// Most important owl features
	items: 1,
	itemsCustom: false,
	itemsDesktop: [1199, 1],
	itemsDesktopSmall: [980, 3],
	itemsTablet: [768, 3],
	itemsTabletSmall: false,
	itemsMobile: [479, 3],
	singleItem: true,
	itemsScaleUp: false,

	//Basic Speeds
	slideSpeed: 1000,
	paginationSpeed: 1000,
	rewindSpeed: 1000,

	//Autoplay
	autoPlay: 10000,
	stopOnHover: false,

	// Navigation
	navigation: true,
	navigationText: ["<i class='fa fa-angle-double-left'>", "<i class='fa fa-angle-double-right'>"],
	rewindNav: false,
	scrollPerPage: false,
	loop: true,
	autoPlay: true,

	//Pagination
	pagination: false,
	paginationNumbers: false,

	// Responsive 
	responsive: true,
	responsiveRefreshRate: 200,
	responsiveBaseWidth: window,

	// CSS Styles
	baseClass: "owl-carousel",
	theme: "owl-theme",

	//Lazy load
	lazyLoad: false,
	lazyFollow: true,
	lazyEffect: "fade",

	//Auto height
	autoHeight: false,

	//JSON 
	jsonPath: false,
	jsonSuccess: false,

	//Mouse Events
	dragBeforeAnimFinish: true,
	mouseDrag: true,
	touchDrag: true,

	//Transitions
	transitionStyle: false,

	// Other
	addClassActive: false,

	//Callbacks
	beforeUpdate: false,
	afterUpdate: false,
	beforeInit: false,
	afterInit: false,
	beforeMove: false,
	afterMove: false,
	afterAction: false,
	startDragging: false,
	afterLazyLoad: false
});


// Video Clip Slider
jQuery(".video-clip-slider").owlCarousel({

	// Most important owl features
	items: 3,
	itemsCustom: false,
	itemsDesktop: [1199, 3],
	itemsDesktopSmall: [980, 3],
	itemsTablet: [768, 3],
	itemsTabletSmall: false,
	itemsMobile: [479, 1],
	singleItem: false,
	itemsScaleUp: false,

	//Basic Speeds
	slideSpeed: 1000,
	paginationSpeed: 1000,
	rewindSpeed: 1000,

	//Autoplay
	autoPlay: 10000,
	stopOnHover: false,

	// Navigation
	navigation: true,
	navigationText: ["<i class='fa fa-angle-left'>", "<i class='fa fa-angle-right'>"],
	rewindNav: true,
	scrollPerPage: false,

	//Pagination
	pagination: false,
	paginationNumbers: false,

	// Responsive 
	responsive: true,
	responsiveRefreshRate: 200,
	responsiveBaseWidth: window,

	// CSS Styles
	baseClass: "owl-carousel",
	theme: "owl-theme",

	//Lazy load
	lazyLoad: false,
	lazyFollow: true,
	lazyEffect: "fade",

	//Auto height
	autoHeight: false,

	//JSON 
	jsonPath: false,
	jsonSuccess: false,

	//Mouse Events
	dragBeforeAnimFinish: true,
	mouseDrag: true,
	touchDrag: true,

	//Transitions
	transitionStyle: false,

	// Other
	addClassActive: false,

	//Callbacks
	beforeUpdate: false,
	afterUpdate: false,
	beforeInit: false,
	afterInit: false,
	beforeMove: false,
	afterMove: false,
	afterAction: false,
	startDragging: false,
	afterLazyLoad: false
});


jQuery(".one-item-slider-paginate").owlCarousel({
	// Most important owl features
	items: 1,
	itemsCustom: false,
	itemsDesktop: [1199, 2],
	itemsDesktopSmall: [980, 1],
	itemsTablet: [768, 1],
	itemsTabletSmall: false,
	itemsMobile: [479, 1],
	singleItem: false,
	itemsScaleUp: false,

	//Basic Speeds
	slideSpeed: 1000,
	paginationSpeed: 1000,
	rewindSpeed: 1000,

	//Autoplay
	autoPlay: 10000,
	stopOnHover: false,

	// Navigation
	navigation: false,
	navigationText: ["<i class='fa fa-angle-left'>", "<i class='fa fa-angle-right'>"],
	rewindNav: true,
	scrollPerPage: false,

	//Pagination
	pagination: true,
	paginationNumbers: false,

	// Responsive 
	responsive: true,
	responsiveRefreshRate: 200,
	responsiveBaseWidth: window,

	// CSS Styles
	baseClass: "owl-carousel",
	theme: "owl-theme",

	//Lazy load
	lazyLoad: false,
	lazyFollow: true,
	lazyEffect: "fade",

	//Auto height
	autoHeight: false,

	//JSON 
	jsonPath: false,
	jsonSuccess: false,

	//Mouse Events
	dragBeforeAnimFinish: true,
	mouseDrag: true,
	touchDrag: true,

	//Transitions
	transitionStyle: false,

	// Other
	addClassActive: false,

	//Callbacks
	beforeUpdate: false,
	afterUpdate: false,
	beforeInit: false,
	afterInit: false,
	beforeMove: false,
	afterMove: false,
	afterAction: false,
	startDragging: false,
	afterLazyLoad: false
});



//CineLokal Slider
jQuery(".movie-slider").owlCarousel({

	// Most important owl features
	items: 4,
	itemsCustom: false,
	itemsDesktop: [1199, 3],
	itemsDesktopSmall: [980, 3],
	itemsTablet: [768, 1],
	itemsTabletSmall: 1,
	itemsMobile: [479, 1],
	singleItem: false,
	itemsScaleUp: false,

	//Basic Speeds
	slideSpeed: 1000,
	paginationSpeed: 1000,
	rewindSpeed: 1000,

	//Autoplay
	autoPlay: 10000,
	stopOnHover: false,

	// Navigation
	navigation: false,
	navigationText: ["<i class='fa fa-angle-double-left'>", "<i class='fa fa-angle-double-right'>"],
	rewindNav: true,
	scrollPerPage: false,

	//Pagination
	pagination: true,
	paginationNumbers: false,

	// Responsive 
	responsive: true,
	responsiveRefreshRate: 200,
	responsiveBaseWidth: window,

	// CSS Styles
	baseClass: "owl-carousel",
	theme: "owl-theme",

	//Lazy load
	lazyLoad: false,
	lazyFollow: true,
	lazyEffect: "fade",

	//Auto height
	autoHeight: false,

	//JSON 
	jsonPath: false,
	jsonSuccess: false,

	//Mouse Events
	dragBeforeAnimFinish: true,
	mouseDrag: true,
	touchDrag: true,

	//Transitions
	transitionStyle: false,

	// Other
	addClassActive: false,

	//Callbacks
	beforeUpdate: false,
	afterUpdate: false,
	beforeInit: false,
	afterInit: false,
	beforeMove: false,
	afterMove: false,
	afterAction: false,
	startDragging: false,
	afterLazyLoad: false
});



//Career Grid Hover
jQuery('.career-grid > li').mouseover(function() {
	jQuery(this).find('.car-title').addClass('ease-top');
	jQuery(this).find('.car-desc').addClass('car-desc-opacity');

	jQuery('.career-grid > li').mouseleave(function() {
		jQuery(this).find('.car-title').removeClass('ease-top');
		jQuery(this).find('.car-desc').removeClass('car-desc-opacity');
	});
});

//Onlocad
windowWidth = jQuery(window).width();
if (windowWidth < 992) {
	jQuery('.career-grid > li').find('.car-title').addClass('ease-top');
	jQuery('.career-grid > li').find('.car-desc').addClass('car-desc-opacity');
}
//On Resize
jQuery(window).resize(function() {
	windowWidth = jQuery(window).width();
	if (windowWidth < 992) {
		jQuery('.career-grid > li').find('.car-title').addClass('ease-top');
		jQuery('.career-grid > li').find('.car-desc').addClass('car-desc-opacity');
	}
});




//Swithch Map Places to Carousel for mobile
jQuery(".place-img").owlCarousel({

	// Most important owl features
	items: 3,
	itemsCustom: false,
	itemsDesktop: [1199, 3],
	itemsDesktopSmall: [980, 3],
	itemsTablet: [768, 3],
	itemsTabletSmall: false,
	itemsMobile: [479, 3],
	singleItem: false,
	itemsScaleUp: false,

	//Basic Speeds
	slideSpeed: 1000,
	paginationSpeed: 1000,
	rewindSpeed: 1000,

	//Autoplay
	autoPlay: 10000,
	stopOnHover: false,

	// Navigation
	navigation: true,
	navigationText: ["<i class='fa fa-angle-double-left'>", "<i class='fa fa-angle-double-right'>"],
	rewindNav: true,
	scrollPerPage: false,

	//Pagination
	pagination: false,
	paginationNumbers: false,

	// Responsive 
	responsive: true,
	responsiveRefreshRate: 200,
	responsiveBaseWidth: window,

	// CSS Styles
	baseClass: "owl-carousel",
	theme: "owl-theme",

	//Lazy load
	lazyLoad: false,
	lazyFollow: true,
	lazyEffect: "fade",

	//Auto height
	autoHeight: false,

	//JSON 
	jsonPath: false,
	jsonSuccess: false,

	//Mouse Events
	dragBeforeAnimFinish: true,
	mouseDrag: true,
	touchDrag: true,

	//Transitions
	transitionStyle: false,

	// Other
	addClassActive: false,

	//Callbacks
	beforeUpdate: false,
	afterUpdate: false,
	beforeInit: false,
	afterInit: false,
	beforeMove: false,
	afterMove: false,
	afterAction: false,
	startDragging: false,
	afterLazyLoad: false
});



//Nowshowing Slider
jQuery(".now-showing-slider").owlCarousel({
	// Most important owl features
	items: 3,
	itemsCustom: false,
	itemsDesktop: [1199, 3],
	itemsDesktopSmall: [980, 1],
	itemsTablet: [768, 1],
	itemsTabletSmall: false,
	itemsMobile: [479, 1],
	singleItem: false,
	itemsScaleUp: false,

	//Basic Speeds
	slideSpeed: 1000,
	paginationSpeed: 1000,
	rewindSpeed: 1000,

	//Autoplay
	autoPlay: 10000,
	stopOnHover: false,

	// Navigation
	navigation: false,
	navigationText: ["<img src='/sites/all/themes/fdcp/images/arrow-left.png'>", "<img src='/sites/all/themes/fdcp/images/arrow-right.png'>"],
	rewindNav: true,
	scrollPerPage: false,

	//Pagination
	pagination: true,
	paginationNumbers: false,

	// Responsive 
	responsive: true,
	responsiveRefreshRate: 200,
	responsiveBaseWidth: window,

	// CSS Styles
	baseClass: "owl-carousel",
	theme: "owl-theme",

	//Lazy load
	lazyLoad: false,
	lazyFollow: true,
	lazyEffect: "fade",

	//Auto height
	autoHeight: false,

	//JSON 
	jsonPath: false,
	jsonSuccess: false,

	//Mouse Events
	dragBeforeAnimFinish: true,
	mouseDrag: true,
	touchDrag: true,

	//Transitions
	transitionStyle: false,

	// Other
	addClassActive: false,

	//Callbacks
	beforeUpdate: false,
	afterUpdate: false,
	beforeInit: false,
	afterInit: false,
	beforeMove: false,
	afterMove: false,
	afterAction: false,
	startDragging: false,
	afterLazyLoad: false
});

//Search 
jQuery('#block-views-exp-movies-page-1 input.form-text').attr('placeholder', 'Search');
jQuery('#block-views-exp-movies-page-1 input.form-submit').attr('value', '🡢');
jQuery('#block-views-exp-movies-page-1').focusin(function() {
	jQuery('#block-views-exp-movies-page-1 input.form-text').css('width', '305px');
	jQuery('#block-block-1').css('opacity', '0');
	jQuery('#block-views-exp-movies-page-1 input.form-submit').css('opacity', '1');
});

jQuery('#block-views-exp-movies-page-1').focusout(function() {
	jQuery('#block-views-exp-movies-page-1 input.form-text').css('width', '90px');
	jQuery('#block-block-1').css('opacity', '1');
	jQuery('#block-views-exp-movies-page-1 input.form-submit').css('opacity', '0');
});

//Login Page add Container
jQuery('#user-login > div').addClass('container');




//Subscribe Placeholder
jQuery('#edit-mergevars-email').attr('placeholder', 'Type your email here')




///BOX OFFICE ADD RANKS
var rank = 1;
jQuery('.rank-number p').each(function(i, obj) {
	if (rank < 6) {
		rank++;
		obj.innerHTML = rank;
	}
});



var top_offset = jQuery('body').offset().top;
// substract half of image height
var top_position = top_offset + (jQuery('body').height() / 2) - 25;

var left_offset = jQuery('body').offset().left;
// substract half of image width
var left_position = left_offset + (jQuery('body').width() / 2) - 25;

// jQuery('.ajax-progress .throbber').css('left', left_position+'px');
// jQuery('.ajax-progress .throbber').css('top', top_position+'px');


jQuery('.ctools-auto-submit-click').is('[disabled=disabled]');

jQuery('.option').click(function() {
	if (jQuery('.ctools-auto-submit-click').is('[disabled=disabled]')) {
		// alert("Hey");
	}
});

// Social MEdia Feeds
jQuery('.else-where a').click(function() { event.preventDefault(); });
jQuery('.show-fb').click(function() { jQuery('.social-content-facebook').css('display', 'block');
	jQuery('.social-content-twitter').css('display', 'none');
	jQuery('.social-content-instagram').css('display', 'none'); });
jQuery('.show-twitter').click(function() { jQuery('.social-content-facebook').css('display', 'none');
	jQuery('.social-content-twitter').css('display', 'block');
	jQuery('.social-content-instagram').css('display', 'none'); });
jQuery('.show-insta').click(function() { jQuery('.social-content-facebook').css('display', 'none');
	jQuery('.social-content-twitter').css('display', 'none');
	jQuery('.social-content-instagram').css('display', 'block'); });


// Change Carousel Navigation Arrow
jQuery( ".section-video-clips .owl-prev").html('<i class="fa fa-angle-left"></i>');
jQuery( ".section-video-clips .owl-next").html('<i class="fa fa-angle-right"></i>');


 jQuery('.video-popup-link').magnificPopup({
          type: 'iframe',
      });

  jQuery('.image-popup-link').magnificPopup({
        type: 'image',
		
      });



// Inview
jQuery(function(){
	var width=jQuery(window).width();
	if(width>='1200'){
		jQuery('.inview-animate').bind('inview',function(event,visible){if(visible){jQuery(this).addClass('animate-iv-obj');}});
	}
	else{	jQuery('.inview-animate').addClass('animate-iv-obj');
	}
});


// Home Map Slide
	jQuery('.trigger-map-1').click(function(){
		jQuery('.embed-map').css('display','none');
		jQuery('.map1').css('width','100%');
		jQuery('.map1').css('display','block');
		jQuery('.address').css('display','none');		
		jQuery('.address1').css('display','block');		
	})
	jQuery('.trigger-map-2').click(function(){
		jQuery('.embed-map').css('display','none');
		jQuery('.map2').css('width','100%');
		jQuery('.map2').css('display','block');
		jQuery('.address').css('display','none');		
		jQuery('.address2').css('display','block');			
	})
	jQuery('.trigger-map-3').click(function(){
		jQuery('.embed-map').css('display','none');
		jQuery('.map3').css('width','100%');	
		jQuery('.map3').css('display','block');
		jQuery('.address').css('display','none');		
		jQuery('.address3').css('display','block');		
	})
	jQuery('.trigger-map-4').click(function(){
		jQuery('.embed-map').css('display','none');
		jQuery('.map4').css('width','100%');	
		jQuery('.map4').css('display','block');
		jQuery('.address').css('display','none');		
		jQuery('.address4').css('display','block');		
	})
	jQuery('.trigger-map-5').click(function(){
		jQuery('.embed-map').css('display','none');
		jQuery('.map5').css('width','100%');	
		jQuery('.map5').css('display','block');
		jQuery('.address').css('display','none');		
		jQuery('.address5').css('display','block');		
	})


	// Disable Breadcrumbs link
	jQuery('.breadcrumb__item a').click(function(){
		event.preventDefault();
	});

});


// Get Facebook For Programs
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


jQuery(document).ready(function(){

	// ScrollTop after webform submit
	jQuery('.registration-form .form-submit').click(function(){	
		jQuery(document).ajaxComplete(function(){
			console.log('ajax complete');
			jQuery("html, body").animate({
	        	scrollTop: jQuery(".messages--error").offset().top
	        });
		});
	});

	// Cinelokal Accordion Display Change images
		// Set MArgin for Accordion Image
		jQuery(window).load(function(){
			if (jQuery(window).width() > 998) {
					var descHeight = jQuery('.page-node-215 .full-description .med-title').outerHeight()+45;
					jQuery('.page-node-215 .full-description .content-right').css('margin-top',descHeight+'px');
			}	
		});

	jQuery('.accordion-image .field-item').each(function(i,n){
		jQuery(this).addClass('accordion-image-'+i);
	});
	jQuery('.page-node-215 .full-description .ui-accordion-header').each(function(i,n){
		jQuery(this).addClass('accordion-title'+i);
		jQuery(this).click(function(){
			jQuery('.accordion-image .field-item').hide();
			jQuery('.accordion-image-'+i).show();
		});
	});

	// jQuery(window).load(function(){
	// 	jQuery('.fic-nav')
	// });
	// jQuery(".fic-nav ul li").click(function(){
	// 	jQuery(this).find('a').click();
	// });
	jQuery(".fic-nav ul li a").on('click', function(e) {
       // prevent default anchor click behavior
       e.preventDefault();
       // animate
       // var offsetVal = jQuery('.fic-nav').outerHeight()+100;
       jQuery('html, body').animate({
           scrollTop: jQuery(this.hash).offset().top-100
         }, 1000, function(){
   
           // when done, add hash to url
           // (default click behaviour)
           window.location.hash = this.hash;
         });
	});
	// jQuery(".fic-nav ul li").click(function(){
	// 	jQuery(".fic-nav ul li").removeClass('active');
	// 	jQuery(this).addClass('active');
	// });

	jQuery(window).load(function(){
		var stickySidebar = jQuery('.fic-nav').offset().top;
		// console.log(stickySidebar);
		jQuery(window).scroll(function() {  
			// 
		    if (jQuery(window).scrollTop() > stickySidebar) {
		        console.log('stick!');
		        jQuery('.fic-nav').addClass('stick-top');
		    }else{
		        jQuery('.fic-nav').removeClass('stick-top');		    	
		    }

		    // 
		    if (jQuery(window).scrollTop() >= jQuery('#fic-sec1').offset().top) {
		    	jQuery('.fic-nav li').removeClass('active');
		    	jQuery('.menu-1').addClass('active');
		    }
		    if (jQuery(window).scrollTop() >= (jQuery('#fic-sec2').offset().top)-120) {
		    	jQuery('.fic-nav li').removeClass('active');
		    	jQuery('.menu-2').addClass('active');
		    }
		    if (jQuery(window).scrollTop() >= (jQuery('#fic-sec3').offset().top)-120) {
		    	jQuery('.fic-nav li').removeClass('active');
		    	jQuery('.menu-3').addClass('active');
		    }


		});	
	});

});





